package ozinoviev.scheduler;

import java.time.*;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Абстрактная реализация {@link DelayedTaskService}
 *
 * @author ozinoviev
 * @implNote Отвечает за подсчет количества активных и ожидающих задач и за преобразование {@link LocalDateTime} в {@link Instant}
 * @since 14.11.18
 */
public abstract class AbstractDelayedTaskService implements DelayedTaskService {

    /**
     * Счетчик ожидающих задач.
     */
    private final AtomicLong pending = new AtomicLong(0);

    /**
     * Счетчик активных задач.
     */
    private final AtomicLong active = new AtomicLong(0);

    /**
     * Возвращает количесво задач, ожидающих выполения.
     *
     * @return количесво задач, ожидающих выполения
     */
    @Override
    public long getPending() {
        return pending.get();
    }

    /**
     * Возвращает количесво задач, выполняющихся в данный момент.
     *
     * @return количесво задач, выполняющихся в данный момент
     */
    @Override
    public long getActive() {
        return active.get();
    }

    /**
     * Добавляет задачу, которая должна быть выполнена в указанное время.
     *
     * @param callable Задача
     * @param at       Время, в которое задача должна быть выполнена
     * @param <T>      Тип вовзращаемого значения
     * @return {@link Future}, который может использоваться для извлечения результата или отмены
     */
    @Override
    public <T> Future<T> schedule(Callable<T> callable, LocalDateTime at) {
        Objects.requireNonNull(callable, "Parameter 'callable' cannot be null");
        Objects.requireNonNull(at, "Parameter 'at' cannot be null");

        var task = new ScheduledTask<>(callable, toInstant(at));
        return scheduleInternal(task);
    }

    /**
     * Добавляет задачу, которая должна быть выполнена в указанное время.
     *
     * @param runnable Задача
     * @param at       Время, в которое задача должна быть выполнена
     * @return {@link Future}, который может использоваться для извлечения результата или отмены
     */
    @Override
    public Future<?> schedule(Runnable runnable, LocalDateTime at) {
        Objects.requireNonNull(runnable, "Parameter 'runnable' cannot be null");
        Objects.requireNonNull(at, "Parameter 'at' cannot be null");

        var task = new ScheduledTask<>(runnable, toInstant(at));
        return scheduleInternal(task);
    }

    /**
     * Добавляет задачу в очередь исполнения
     *
     * @param task Задача
     * @param <T>  Тип возвращаемого значения
     * @return {@link Future}, который может использоваться для извлечения результата или отмены
     */
    protected abstract <T> Future<T> schedule(ScheduledTask<T> task);

    /**
     * Удаляет задачу из очереди исполнения
     *
     * @param task Задача
     * @return true - если задача была удалена из очереди, иначе false
     */
    protected abstract boolean cancel(ScheduledTask<?> task);

    /**
     * Добавляет задачу в очередь исполнения и инкрементирует {@link #pending}
     */
    private <T> Future<T> scheduleInternal(ScheduledTask<T> task) {
        var future = schedule(task);
        pending.incrementAndGet();
        return future;
    }

    /**
     * Удаляет задачу из очередь исполнения и декрементирует {@link #pending}
     */
    private void cancelInternal(ScheduledTask<?> task) {
        if (cancel(task)) {
            pending.decrementAndGet();
        }
    }

    /**
     * Преобразует переданный {@link LocalDateTime} в {@link Instant} в системной временной зоне
     */
    private Instant toInstant(LocalDateTime dateTime) {
        var zoneId = ZoneOffset.systemDefault();
        return dateTime.atZone(zoneId).toInstant();
    }

    final class ScheduledTask<T> extends FutureTask<T> {
        private final Instant at;

        private ScheduledTask(Callable<T> callable,
                              Instant at) {
            super(callable);
            this.at = at;
        }

        private ScheduledTask(Runnable runnable,
                              Instant at) {
            super(runnable, null);
            this.at = at;
        }

        Instant at() {
            return at;
        }

        @Override
        public void run() {
            try {
                pending.decrementAndGet();
                active.incrementAndGet();
                super.run();
            } finally {
                active.decrementAndGet();
            }
        }

        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            if (super.cancel(mayInterruptIfRunning)) {
                cancelInternal(this);
                return true;
            }

            return false;
        }
    }

}
