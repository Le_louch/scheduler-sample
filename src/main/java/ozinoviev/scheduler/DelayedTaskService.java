package ozinoviev.scheduler;

import java.time.LocalDateTime;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/**
 * Интерфейс сервиса для выполнения заданий в указанное время.
 *
 * @author ozinoviev
 * @since 14.11.18
 */
@SuppressWarnings("unused")
public interface DelayedTaskService extends AutoCloseable {

    /**
     * Добавляет задачу, которая должна быть выполнена в указанное время.
     *
     * @param callable Задача
     * @param at       Время, в которое задача должна быть выполнена
     * @param <T>      Тип вовзращаемого значения
     * @return {@link Future}, который может использоваться для извлечения результата или отмены
     */
    <T> Future<T> schedule(Callable<T> callable, LocalDateTime at);

    /**
     * Добавляет задачу, которая должна быть выполнена в указанное время.
     *
     * @param runnable Задача
     * @param at       Время, в которое задача должна быть выполнена
     * @return {@link Future}, который может использоваться для извлечения результата или отмены
     */
    Future<?> schedule(Runnable runnable, LocalDateTime at);

    /**
     * Возвращает количесво задач, ожидающих выполения.
     *
     * @return количесво задач, ожидающих выполения
     */
    long getPending();

    /**
     * Возвращает количесво задач, выполняющихся в данный момент.
     *
     * @return количесво задач, выполняющихся в данный момент
     */
    long getActive();

    /**
     * Завершает работу сервиса.
     *
     * @param waitTermination Ожидать ли завершения потоков
     */
    void close(boolean waitTermination);

    /**
     * Завершает работу сервиса и ожидает завершения потоков.
     */
    default void close() {
        close(true);
    }

}
