package ozinoviev.scheduler;

import java.time.Instant;
import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Реализация {@link DelayedTaskService}, использующая кастомный пулл потоков и {@link DelayQueue}
 *
 * @author ozinoviev
 * @since 14.11.18
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public final class DelayedTaskServiceImpl
        extends AbstractDelayedTaskService
        implements DelayedTaskService {

    private final AtomicLong current = new AtomicLong(0);
    private final DelayQueue<DelayedRunnable> queue;
    private final SimpleThreadPool pool;

    /**
     * Создает новый экземпляр с количеством потоков равным количеству ядер процессора.
     */
    public DelayedTaskServiceImpl() {
        this(Runtime.getRuntime().availableProcessors());
    }

    /**
     * Создает новый экземпляр с указанным количеством потоков.
     *
     * @param threads Количество потоков
     */
    public DelayedTaskServiceImpl(int threads) {
        this(threads, new SimpleThreadFactory("DelayedSchedule", true));
    }

    /**
     * Создает новый экземпляр с указанным количеством и фабрикой потоков.
     *
     * @param threads Количество потоков
     * @param factory Фабрика потоков
     */
    public DelayedTaskServiceImpl(int threads, ThreadFactory factory) {
        if (threads <= 0) {
            throw new IllegalArgumentException("Parameter 'threads' cannot be less or equal to zero");
        }

        Objects.requireNonNull(factory, "Parameter 'factory' cannot be null");

        this.queue = new DelayQueue<>();
        this.pool = new SimpleThreadPool(threads, queue, factory);
    }

    /**
     * Добавляет задачу в очередь исполнения
     *
     * @param task Задача
     * @param <T>  Тип возвращаемого значения
     * @return {@link Future}, который может использоваться для извлечения результата или отмены
     */
    @Override
    protected <T> Future<T> schedule(ScheduledTask<T> task) {
        queue.offer(new FIFOTask(new DelayedTask(task), current.getAndIncrement()));
        return task;
    }

    /**
     * Удаляет задачу из очереди исполнения
     *
     * @param task Задача
     * @return true - если задача была удалена из очереди, иначе false
     */
    @Override
    protected boolean cancel(ScheduledTask<?> task) {
        return queue.remove(new FIFOTask(new DelayedTask(task), 0));
    }

    /**
     * Завершает работу сервиса.
     *
     * @param waitTermination Ожидать ли завершения потоков
     */
    @Override
    public void close(boolean waitTermination) {
        pool.shutdown(waitTermination);
    }

    /**
     * Интерфейс задачи, момент выполнения которой должен быть отложен
     */
    private interface DelayedRunnable extends Delayed, Runnable {
    }

    /**
     * Задача, момент выполнения которой должен быть отложен
     */
    private final static class DelayedTask implements DelayedRunnable {

        private final ScheduledTask<?> task;
        private final long timestamp;

        private DelayedTask(ScheduledTask<?> task) {
            this.task = task;
            this.timestamp = task.at().toEpochMilli();
        }

        /**
         * Период времени, на который должно быть отложено выполнение задачи.
         *
         * @param unit {@link TimeUnit}
         * @return Период времени в указанном {@link TimeUnit}
         */
        @Override
        public long getDelay(TimeUnit unit) {
            Objects.requireNonNull(unit);
            return unit.convert(timestamp - Instant.now().toEpochMilli(), TimeUnit.MILLISECONDS);
        }

        /**
         * Сравнивает время выполнения текущей {@link DelayedTask} с переданной {@link DelayedTask}
         *
         * @param o {@link DelayedTask}
         * @return Результат сравнения
         */
        @Override
        public int compareTo(Delayed o) {
            Objects.requireNonNull(o);

            if (o instanceof DelayedTask) {
                var other = (DelayedTask) o;
                return task.at().compareTo(other.task.at());
            }

            throw new IllegalStateException("Task queue contains unexpected task type " + o.getClass());
        }

        @Override
        public void run() {
            task.run();
        }

        /**
         * @implNote Для того, чтобы корректно удалить задачу из очереди, реализация должна опираться только на значение {@link #task}
         */
        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }

            if (o instanceof DelayedTask) {
                DelayedTask that = (DelayedTask) o;
                return Objects.equals(task, that.task);
            }

            return false;
        }

        /**
         * @implNote Для того, чтобы корректно удалить задачу из очереди, реализация должна опираться только на значение {@link #task}
         */
        @Override
        public int hashCode() {
            return Objects.hash(task);
        }
    }

    /**
     * @implNote Для задач, добавленных с одним временем исполнения необходимо добиться исполения в порядке добавелния.
     * Так как спецификация {@link DelayQueue} нам этого не гарантирует, оборачиваем {@link DelayedRunnable} в объект
     * с информацией об индексе вставки в очередь.
     */
    private final static class FIFOTask implements DelayedRunnable {

        private final DelayedRunnable runnable;
        private final long index;

        private FIFOTask(DelayedRunnable runnable,
                         long index) {
            this.runnable = runnable;
            this.index = index;
        }

        @Override
        public long getDelay(TimeUnit unit) {
            return runnable.getDelay(unit);
        }

        /**
         * Сравнивает время выполнения текущей {@link FIFOTask} с переданной {@link FIFOTask}
         *
         * @param o {@link FIFOTask}
         * @return Результат сравнения
         * @implNote Делегируем сравнение в {@link #runnable}. Если результат сравнения равен 0, то сравниваем индексы добавления задач.
         * @// TODO: 15.11.18 Предусмотреть обработку переполнения long
         */
        @Override
        public int compareTo(Delayed o) {
            Objects.requireNonNull(o);

            if (o instanceof FIFOTask) {
                var other = (FIFOTask) o;
                var result = runnable.compareTo(other.runnable);
                if (result == 0) {
                    return Long.compareUnsigned(index, other.index);
                }

                return result;
            }

            throw new IllegalStateException("Task queue contains unexpected task type " + o.getClass());
        }

        @Override
        public void run() {
            runnable.run();
        }

        /**
         * @implNote Для того, чтобы корректно удалить задачу из очереди, реализация должна опираться только на значение {@link #runnable}
         */
        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }

            if (o instanceof FIFOTask) {
                FIFOTask that = (FIFOTask) o;
                return Objects.equals(runnable, that.runnable);
            }

            return false;
        }

        /**
         * @implNote Для того, чтобы корректно удалить задачу из очереди, реализация должна опираться только на значение {@link #runnable}
         */
        @Override
        public int hashCode() {
            return Objects.hash(runnable);
        }
    }

    /**
     * Простая реализация пула потоков.
     * @implNote Просто создает коллекцию потоков, которые пытаются взять задачу из очереди и выполнить ее.
     */
    private final static class SimpleThreadPool {
        private final Collection<Thread> workerThreads;
        private final BlockingQueue<? extends Runnable> queue;
        private final AtomicBoolean terminated = new AtomicBoolean(false);

        private SimpleThreadPool(final int threads,
                                 final BlockingQueue<? extends Runnable> queue,
                                 final ThreadFactory threadFactory) {
            this.queue = queue;
            this.workerThreads = IntStream.range(0, threads)
                    .mapToObj(ignored -> new Worker())
                    .map(threadFactory::newThread)
                    .peek(Thread::start)
                    .collect(Collectors.toSet());
        }

        private void shutdown(boolean waitTermination) {
            this.terminated.set(true);
            this.workerThreads.forEach(Thread::interrupt);
            if (waitTermination) {
                while (workerThreads.stream().anyMatch(Thread::isAlive)) {
                    Thread.yield();
                }
            }
        }

        private final class Worker implements Runnable {

            @Override
            public void run() {
                while (!terminated.get()) {
                    try {
                        var runnable = queue.take();
                        try {
                            runnable.run();
                        } catch (Exception e) {
                            //do nothing
                        }
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            }
        }
    }

    private final static class SimpleThreadFactory implements ThreadFactory {

        private final ThreadGroup group;
        private final boolean daemon;

        private SimpleThreadFactory(String groupName,
                                    boolean daemon) {
            this.group = new ThreadGroup(groupName);
            this.group.setDaemon(true);
            this.daemon = daemon;
        }

        @Override
        public Thread newThread(Runnable r) {
            var thread = new Thread(group, r);
            thread.setDaemon(daemon);
            return thread;
        }
    }

}
