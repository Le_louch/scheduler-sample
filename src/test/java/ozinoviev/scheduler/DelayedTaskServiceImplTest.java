package ozinoviev.scheduler;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

/**
 * @author ozinoviev
 * @since 14.11.18
 */
public class DelayedTaskServiceImplTest {

    @Test
    public void testSchedule() throws InterruptedException, ExecutionException {
        final AtomicInteger counter = new AtomicInteger();

        try (var service = new DelayedTaskServiceImpl()) {
            var f1 = service.schedule(counter::incrementAndGet, LocalDateTime.now().plusSeconds(1));
            var f2 = service.schedule(counter::incrementAndGet, LocalDateTime.now().plusSeconds(2));

            Thread.sleep(250);
            Assert.assertFalse(f1.isDone());
            Assert.assertNotEquals(1, counter.get());

            f1.get();
            Assert.assertEquals(1, counter.get());

            Thread.sleep(250);
            Assert.assertFalse(f2.isDone());
            Assert.assertNotEquals(2, counter.get());

            f2.get();
            Assert.assertEquals(2, counter.get());
        }
    }

    @Test
    public void testOrder() throws InterruptedException, ExecutionException {

        try (var service = new DelayedTaskServiceImpl(1)) {
            final AtomicInteger value = new AtomicInteger(0);
            LocalDateTime at = LocalDateTime.now().plusSeconds(1);
            var f1 = service.schedule(() -> value.compareAndSet(0, 1), at);
            var f2 = service.schedule(() -> value.compareAndSet(1, -1), at);
            var f3 = service.schedule(() -> value.compareAndSet(-1, 2), at);
            var f4 = service.schedule(() -> value.compareAndSet(2, -2), at);

            f4.get();
            Assert.assertEquals(-2, value.get());
            Assert.assertTrue(f1.isDone());
            Assert.assertTrue(f2.isDone());
            Assert.assertTrue(f3.isDone());
        }
    }

    @Test
    public void testPool() throws InterruptedException, ExecutionException {

        final AtomicInteger value = new AtomicInteger(0);

        Function<Integer, Runnable> factory = (wait) -> () -> {
            try {
                Thread.sleep(wait);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }

            value.set(wait);
        };

        try (var service = new DelayedTaskServiceImpl(2)) {

            LocalDateTime now = LocalDateTime.now();
            var f1 = service.schedule(factory.apply(2000), now);
            var f2 = service.schedule(factory.apply(100), now.plusSeconds(1));

            f2.get();
            Assert.assertEquals(100, value.get());
            Assert.assertFalse(f1.isDone());

            f1.get();
            Assert.assertEquals(2000, value.get());
        }
    }

    @Test
    public void testUnordered() throws ExecutionException, InterruptedException {
        try (var service = new DelayedTaskServiceImpl(1)) {
            final AtomicInteger value = new AtomicInteger(0);
            LocalDateTime at = LocalDateTime.now().plusSeconds(1);
            var f1 = service.schedule(() -> value.compareAndSet(0, 1), at.plusSeconds(1));
            var f2 = service.schedule(() -> value.compareAndSet(0, -1), at.minusSeconds(2));

            f1.get();
            f2.get();
            Assert.assertEquals(-1, value.get());
            Assert.assertFalse(f1.get());
        }
    }

}
